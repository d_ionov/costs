package ru.ionovsoft.costs.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.ionovsoft.costs.dao.pojo.Balance;

import java.util.HashMap;
import java.util.Map;

@Controller
public class MainController {

    @Autowired
    private Balance balance;

    @GetMapping("/")
    public ModelAndView getIndex(){
        Map<String, String> model = new HashMap<>();
        model.put("balance", String.valueOf(balance.getBalance()));
        return new ModelAndView("index", model);
    }

}
