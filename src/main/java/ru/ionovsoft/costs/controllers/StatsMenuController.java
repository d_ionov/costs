package ru.ionovsoft.costs.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class StatsMenuController {

    @GetMapping("/stats")
    public ModelAndView getStatsPage(){
        return new ModelAndView("statsmenu");
    }

    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public String getTransactionsByDate(HttpServletRequest request){
        String date = request.getParameter("date");
        return null;
    }


}
