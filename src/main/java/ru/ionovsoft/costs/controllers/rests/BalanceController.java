package ru.ionovsoft.costs.controllers.rests;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ionovsoft.costs.dao.pojo.Balance;
import ru.ionovsoft.costs.dao.pojo.History;
import ru.ionovsoft.costs.dao.pojo.operation.Payment;
import ru.ionovsoft.costs.dao.pojo.operation.PaymentsTypes;
import ru.ionovsoft.costs.dao.pojo.operation.Receipts;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RestController
@RequestMapping("/balance")
public class BalanceController {

    @Autowired
    Balance balance;

    @Autowired
    History history;

    private final static Gson gson = new Gson();

    @RequestMapping(value = "/put", method = RequestMethod.POST)
    public void putMoney(@RequestParam String sum, @RequestParam String description){
        balance.put(Integer.valueOf(sum));
        history.addTransaction(new Receipts(Integer.valueOf(sum), description, new Date()));
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getBalance(){
        String balanceJson = gson.toJson(balance);
        return balanceJson;
    }

    @RequestMapping(value = "/take", method = RequestMethod.POST)
    public void takeMoney(@RequestParam String sum, @RequestParam String description, @RequestParam String inputType){
        PaymentsTypes type = PaymentsTypes.valueOf(inputType);
        balance.take(Integer.valueOf(sum));
        history.addTransaction(new Payment(Integer.valueOf(sum), new Date(), description, type));
    }

}
