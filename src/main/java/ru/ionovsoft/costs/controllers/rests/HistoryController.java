package ru.ionovsoft.costs.controllers.rests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.ionovsoft.costs.dao.pojo.History;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/history")
public class HistoryController {

    @Autowired
    private History history;

    @RequestMapping("/")
    public String getHistory(){
        return history.toString();
    }

}
