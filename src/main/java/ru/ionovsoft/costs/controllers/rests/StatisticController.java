package ru.ionovsoft.costs.controllers.rests;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.ionovsoft.costs.dao.pojo.History;
import ru.ionovsoft.costs.dao.pojo.operation.Operation;
import ru.ionovsoft.costs.dao.pojo.operation.Payment;
import ru.ionovsoft.costs.dao.pojo.operation.PaymentsTypes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@RestController
@RequestMapping("/stats")
public class StatisticController {

    @Autowired
    private History history;

    @RequestMapping(value = "/history", method = RequestMethod.GET)
    public String getHistory(){
        return history.toString();
    }

    @RequestMapping(value = "/history/period", method = RequestMethod.POST)
    public String getHistoryForPeriod(HttpServletRequest request){
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        return history.getHistory().subMap(startDate,true, endDate, true).toString();
    }

    @RequestMapping(value = "/relation", method = RequestMethod.GET)
    public String getPercentRelation(){
        TreeMap<String, List<Operation>> map = history.getHistory();
        return getRelation(map);
    }

    @RequestMapping(value = "/relation", method = RequestMethod.POST)
    public String getPercentRelationForPeriod(HttpServletRequest request){
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        TreeMap<String, List<Operation>> map = history.getHistory();
        Map<String, List<Operation>> subMap = map.subMap(startDate, true, endDate, true);
        return getRelation(subMap);

    }

    private String getRelation(Map<String, List<Operation>> map){
        final int[] general = {0};
        final int[] optional = {0};
        map.entrySet().stream().forEach(entry -> {List<Operation> list = entry.getValue();
            for(Operation operation : list){
                if(operation instanceof Payment){
                    if(((Payment) operation).getType() == PaymentsTypes.General){
                        general[0] += operation.getSum();
                    }else{
                        optional[0] += operation.getSum();
                    }
                }
            }
        });
        int sum = general[0] + optional[0];
        int generalPercent = general[0] * 100 / sum;
        int optionalPercent = optional[0] * 100 / sum;
        return "General: " + generalPercent + "%("+general[0]+"рублей) " + " Optional: " + optionalPercent + "%("+optional[0]+"рублей)";
    }

}
