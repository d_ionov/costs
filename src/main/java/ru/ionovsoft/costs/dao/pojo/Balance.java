package ru.ionovsoft.costs.dao.pojo;


import org.springframework.stereotype.Component;

@Component
public class Balance {

    private int balance = 0;

    public int getBalance() {
        return balance;
    }

    public void put(int sum){
        balance +=sum;
    }

    public void take(int sum){
        balance -=sum;
    }

}
