package ru.ionovsoft.costs.dao.pojo;

import lombok.Getter;
import lombok.ToString;
import org.springframework.stereotype.Component;
import ru.ionovsoft.costs.dao.pojo.operation.Operation;
import ru.ionovsoft.costs.dao.pojo.operation.Payment;
import ru.ionovsoft.costs.dao.pojo.operation.PaymentsTypes;

import java.text.SimpleDateFormat;
import java.util.*;

@Component
@ToString
public class History {

    @Getter
    private final TreeMap<String, List<Operation>> history = new TreeMap<>();

    public History(){}

    public void addTransaction(Operation operation){
        Date dateNow = operation.getDate()  ;
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("yyyy-MM-dd");
        String stringDate = formatForDateNow.format(dateNow);
        List<Operation> transactions = history.get(stringDate);
        if(transactions != null){
            transactions.add(operation);
        }else{
            transactions = new ArrayList<>();
            transactions.add(operation);
            history.put(stringDate, transactions);
        }

    }

}
