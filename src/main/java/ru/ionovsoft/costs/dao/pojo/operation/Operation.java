package ru.ionovsoft.costs.dao.pojo.operation;

import java.util.Date;

public interface Operation {

    Date getDate();

    int getSum();

    String getDescription();

}
