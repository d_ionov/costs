package ru.ionovsoft.costs.dao.pojo.operation;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;

@ToString
@EqualsAndHashCode
public class Payment implements Operation{

    @Getter
    private int sum;

    @Getter
    private Date date;

    @Getter
    private String description;

    @Getter
    private PaymentsTypes type;

    public Payment(int sum, Date date, String description, PaymentsTypes type) {
        this.sum = sum;
        this.date = date;
        this.description = description;
        this.type = type;
    }

}
