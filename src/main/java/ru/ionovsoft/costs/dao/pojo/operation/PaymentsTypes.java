package ru.ionovsoft.costs.dao.pojo.operation;

import lombok.Getter;

public enum PaymentsTypes {

    General("General"), Optional("Optional");

    @Getter
    private String type;

    PaymentsTypes(String type) {
        this.type = type;
    }
}
