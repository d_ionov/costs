package ru.ionovsoft.costs.dao.pojo.operation;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;

@ToString
@EqualsAndHashCode
public class Receipts implements Operation{

    @Getter
    private int sum;

    @Getter
    private String description;

    @Getter
    private Date date;

    public Receipts(int sum, String description, Date date) {
        this.sum = sum;
        this.description = description;
        this.date = date;
    }


}
